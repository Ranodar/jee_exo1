<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: abbou
  Date: 07/12/2022
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste des produits</title>
</head>
<body>
<div>
    <form method="post" action="produits">
        <label for="dateAchat">Date d'achat : </label>
        <input type="text" name="dateAchat" id="dateAchat">
        <label for="marque">Marque : </label>
        <input type="text" name="marque" id="marque">
        <label for="prix">Prix : </label>
        <input type="text" name="prix" id="prix">
        <label for="reference">Référence : </label>
        <input type="text" name="reference" id="reference">
        <label for="stock">Stock : </label>
        <input type="text" name="stock" id="stock">
        <input type="submit"/>
    </form>
    Liste des produits
    <div>
        <table>
            <tr>
                <th>Id</th>
                <th>Marque</th>
                <th>Détails</th>
            </tr>
            <c:forEach items="${produits}" var ="produit">
                <tr>
                    <td>${produit.getId()}</td>
                    <td>${produit.getMarque()}</td>
                    <td><a href="produits?id=${produit.getId()}">Détails</a></td>
                </tr>
            </c:forEach>
        </table>
    </div>

</body>
</html>
