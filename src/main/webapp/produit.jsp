<%--
  Created by IntelliJ IDEA.
  User: abbou
  Date: 07/12/2022
  Time: 11:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>DétailsProduit</title>
</head>
<body>
<div>
    Détails du produit
    <table>
        <tr>
            <th>Id</th>
            <th>Date d'achat</th>
            <th>Marque</th>
            <th>Prix</th>
            <th>Référence</th>
            <th>Stock</th>
        </tr>
            <tr>
                <td>${produit.getId()}</td>
                <td>${produit.getDateAchat()}</td>
                <td>${produit.getMarque()}</td>
                <td>${produit.getPrix()}</td>
                <td>${produit.getReference()}</td>
                <td>${produit.getStock()}</td>
            </tr>
    </table>
</div>

</body>
</html>
