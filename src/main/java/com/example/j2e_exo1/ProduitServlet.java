package com.example.j2e_exo1;

import com.example.j2e_exo1.entities.Produit;
import com.example.j2e_exo1.services.ProduitService;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet(name = "produitServlet", value = "/produits")
public class ProduitServlet extends HttpServlet {

    private List<Produit> produits;
    private ProduitService produitService;

    public void init() {
        produitService = new ProduitService();
        produits = produitService.findAll();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getParameter("id") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            Produit produit = null;
            try {
                produit = produitService.findById(id);
            } catch (Exception e) {

            }

            if (produit != null) {
                request.setAttribute("produit", produit);
                request.getRequestDispatcher("produit.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("erreur.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("produits", produits);
            request.getRequestDispatcher("produits.jsp").forward(request, response);
        }
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        if (!request.getParameter("marque").isBlank()
                && !request.getParameter("reference").isBlank()
                && !request.getParameter("dateAchat").isBlank()
                && !request.getParameter("prix").isBlank()
                && !request.getParameter("stock").isBlank()) {

            String marque = request.getParameter("marque");
            String reference = request.getParameter("reference");
            Date dateAchat = null;
            try {
                dateAchat = new SimpleDateFormat("dd/mm/yyyy").parse(request.getParameter("dateAchat"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            double prix = Double.parseDouble(request.getParameter("prix"));
            int stock = Integer.parseInt(request.getParameter("stock"));

            Produit produit = new Produit(marque, reference, prix, stock);
            if (produitService.create(produit)) {
                request.setAttribute("produits",produits);
                request.getRequestDispatcher("produits.jsp").forward(request,response);
            } else {
                request.getRequestDispatcher("erreur.jsp").forward(request, response);
            }
        } else {
            request.getRequestDispatcher("erreur.jsp").forward(request, response);
        }

    }


}
